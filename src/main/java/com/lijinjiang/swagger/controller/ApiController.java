package com.lijinjiang.swagger.controller;

import com.lijinjiang.swagger.common.Result;
import com.lijinjiang.swagger.entity.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description API控制器
 * @Author 03010430
 * @ModifyDate 2023/5/26 15:29
 */
@RestController
@RequestMapping("/api/test/v1")
public class ApiController {
    @GetMapping("/users")
    public Result listUsers() {
        List<User> list = new ArrayList<>();
        list.add(new User("1","张三","123456"));
        list.add(new User("2","李四","123456"));
        list.add(new User("3","王二","123456"));
        return Result.success(list);
    }
}
