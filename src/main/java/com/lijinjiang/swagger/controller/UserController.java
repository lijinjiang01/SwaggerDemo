package com.lijinjiang.swagger.controller;

import com.lijinjiang.swagger.common.Result;
import com.lijinjiang.swagger.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * @Description 用户控制器
 * @Author 03010430
 * @ModifyDate 2023/2/13 14:56
 */
@Api(tags = {"用户管理"})
@RestController
public class UserController {

    @ApiOperation(value = "新增用户", notes = "根据传入的数据，新增用户", produces = "application/json")
    @PostMapping("/users")
    public Result addUser(@RequestBody User user) {
        //具体业务代码
        return Result.success(null);
    }

    @ApiOperation(value = "删除用户", notes = "根据传入的id，删除对应用户")
    @DeleteMapping("/users/{id}")
    public Result deleteUser(@PathVariable("id") String id) {
        //具体业务代码
        return Result.success(null);
    }

    @ApiOperation(value = "修改用户", notes = "根据传入的数据，修改用户")
    @PutMapping("/users")
    public Result updateUser(@RequestBody User user) {
        //具体业务代码
        return Result.success(null);
    }

    @ApiOperation(value = "查询用户", notes = "根据传入的id，查找返回对应用户信息")
    @GetMapping("/users/{id}")
    public Result queryUser(@PathVariable("id") String id) {
        //具体业务代码
        return Result.success(null);
    }
}
