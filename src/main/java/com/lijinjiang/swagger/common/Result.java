package com.lijinjiang.swagger.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description 返回结果通用类
 * @Author 03010430
 * @ModifyDate 2023/2/13 15:13
 */
@Data
@ApiModel(description = "返回结果通用类")
public class Result<T> implements Serializable {
    public static final long serialVersionUID = 1L;
    //返回状态码
    @ApiModelProperty(value = "返回状态码")
    private int code;
    //返回消息
    @ApiModelProperty(value = "返回消息")
    private String msg;
    //返回数据
    @ApiModelProperty(value = "返回数据")
    private T data;

    //成功操作
    public static <T> Result<T> success(T object) {
        Result<T> result = new Result<>();
        result.setCode(200);
        result.setMsg("操作成功");
        result.setData(object);
        return result;
    }

    //失败操作
    public static <T> Result<T> failure(String msg) {
        Result<T> result = new Result<>();
        result.setCode(500);
        result.setMsg(msg);
        return result;
    }

}
