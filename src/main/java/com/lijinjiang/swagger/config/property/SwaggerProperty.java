package com.lijinjiang.swagger.config.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Description Swagger接口文档配置类
 * @Author 03010430
 * @ModifyDate 2023/2/13 14:36
 */
@Data
@ConfigurationProperties("swagger")
public class SwaggerProperty {
    //系统标题
    private String title;
    //分组名称
    private String groupName;
    //是否开启
    private Boolean enable = true;
    //描述信息
    private String describe;
    //版本信息
    private String version;
    //扫描路径
    private String scanPackage;
    //组织链接
    private String termsOfServiceUrl;
    //联系人名称
    private String contactName;
    //联系人url
    private String contactUrl;
    //联系人email
    private String contactEmail;
}
