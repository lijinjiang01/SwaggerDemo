package com.lijinjiang.swagger.config;

import com.lijinjiang.swagger.config.property.SwaggerProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @Description Swagger配置文件
 * @Author 03010430
 * @ModifyDate 2023/2/13 14:35
 */
@Configuration
@EnableConfigurationProperties(SwaggerProperty.class)
public class SwaggerConfig {

    @Resource
    private SwaggerProperty swaggerProperty;

    @Bean
    public Docket createApiDoc() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName(swaggerProperty.getGroupName())
                .enable(swaggerProperty.getEnable())
                .select()
                .apis(RequestHandlerSelectors.basePackage(swaggerProperty.getScanPackage()))
                .build().directModelSubstitute(Timestamp.class, Date.class);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(swaggerProperty.getTitle())
                .description(swaggerProperty.getDescribe())
                .version(swaggerProperty.getVersion())
                .termsOfServiceUrl(swaggerProperty.getTermsOfServiceUrl())
                .contact(new Contact(swaggerProperty.getContactName(), swaggerProperty.getContactUrl()
                        , swaggerProperty.getContactEmail()))
                .build();
    }
}
