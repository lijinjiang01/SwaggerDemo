package com.lijinjiang.swagger.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Description 用户类
 * @Author 03010430
 * @ModifyDate 2023/2/13 14:52
 */
@Data
@ApiModel(description = "用户类")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value="主键ID")
    private String id;
    @ApiModelProperty(value="账号")
    private String account;
    @ApiModelProperty(value="密码")
    private String password;
    @ApiModelProperty(value="头像")
    private String avatar;
    @ApiModelProperty(value="电话")
    private String phone;
    @ApiModelProperty(value="创建时间")
    private Timestamp ctime;

    public User() {
    }

    public User(String id, String account, String password) {
        this.id = id;
        this.account = account;
        this.password = password;
    }
}
